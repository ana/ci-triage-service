#!/usr/bin/env python3
#
# Copyright © 2021 Google LLC
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.

import argparse
import io
import re
import socket
import time


class Connection:
    def __init__(self, host, port, verbose):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.connect((host, port))
        self.s.setblocking(0)
        self.verbose = verbose

    def send_line(self, line):
        if self.verbose:
            print(f"IRC: sending {line}")
        self.s.sendall((line + '\n').encode())

    def wait(self, secs):
        for i in range(secs):
            if self.verbose:
                while True:
                    try:
                        data = self.s.recv(1024)
                    except io.BlockingIOError:
                        break
                    if data == "":
                        break
                    for line in data.decode().split('\n'):
                        print(f"IRC: received {line}")
            time.sleep(1)

    def quit(self):
        self.send_line("QUIT")
        time.sleep(1)
        self.s.shutdown(socket.SHUT_WR)
        self.s.close()


class IRCClient:
    def __init__(self, host, port, nick, channel):
        self.channel = channel

        self.irc = Connection(host, port, verbose=True)

        # The nick needs to be something unique so that multiple runners
        # connecting at the same time don't race for one nick and get blocked.
        # freenode has a 16-char limit on nicks (9 is the IETF standard, but
        # various servers extend that).  So, trim off the common prefixes of the
        # runner name, and append the job ID so that software runners with more
        # than one concurrent job (think swrast) don't collide.  For freedreno,
        # that gives us a nick as long as db410c-N-JJJJJJJJ, and it'll be a while
        # before we make it to 9-digit jobs (we're at 7 so far).
        self.irc.send_line(f"NICK {nick}")
        self.irc.send_line(f"USER {nick} unused unused: Valve Monitor Notifier")
        self.irc.wait(10)
        self.irc.send_line(f"JOIN {channel}")
        self.irc.wait(1)

    def send_line(self, line):
        if line:
            self.irc.send_line(f"PRIVMSG {self.channel} :{line}")

    def send_msg(self, msg):
        for line in msg.splitlines():
            self.irc.send_line(f"PRIVMSG {self.channel} :{line}")
            time.sleep(1)

    def quit(self):
        self.irc.quit()
