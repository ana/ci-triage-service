from dataclasses import dataclass, field
from functools import cached_property
from enum import IntFlag, auto
import io
import re
from typing import Optional


@dataclass
class MatcherAction:
    name: str
    description: str
    regex: re.Pattern
    requires_attention: bool = False

    def __hash__(self):
        return hash(self.name)

    def match_line(self, line):
        if m := self.regex.search(line):
            return Match(self, m)


@dataclass
class Match:
    matcher: MatcherAction
    match: re.Match


MATCHER_TABLE = [
    MatcherAction(
        name="AMDGPU_ENOMEM",
        description="Amdgpu ran out of space for command submission",
        regex=re.compile(r"\*ERROR\* Not enough memory for command submission!"),
    ),
    MatcherAction(
        name="BOOT_FAILURE",
        description="First console activity timeout",
        regex=re.compile(r"Hit the timeout <Timeout first_console_activity.*--> Abort!"),
        requires_attention=True,
    ),
    MatcherAction(
        name="CONTAINER_ERROR",
        description="Container build / runtime issue",
        regex=re.compile(
            r"error committing container|error pushing image|Error processing tar file|http: server gave HTTP response to HTTPS client|writing blob: adding layer with blob .*: layer not known|Cannot connect to the Docker daemon at unix:///var/run/docker\.sock\. Is the docker daemon running\?"
        ),
    ),
    MatcherAction(
        name="DEQP_PARSE_ERROR",
        description="dEQP caselists parse error",
        regex=re.compile(r"Error: Failed to parse dEQP"),
    ),
    MatcherAction(
        name="DRM_TIMEDOUT_FAILURE",
        description="Test suite timed due to device or kernel failure",
        regex=re.compile(r"\[drm:amdgpu_job_timedout\]|timed out waiting for fd to be ready|\\*ERROR\\* ring.*timeout"),
    ),
    MatcherAction(
        name="EXECUTOR_ISSUE",
        description="Executor bug / issue",
        regex=re.compile(r"ValueError: The server failed to initiate a connection|Internal Server Error"),
        requires_attention=True,
    ),
    MatcherAction(
        name="EXECUTOR_TIMEOUT",
        description="One of the timeouts got hit",
        regex=re.compile(r"Hit the timeout <Timeout"),
        requires_attention=True,
    ),
    MatcherAction(
        name="GITLAB_GENERIC_JOB_FAILURE",
        description="Gitlab related infra failure",
        regex=re.compile(
            r'requests.exceptions.HTTPError: 404 Client Error: Not Found for url: https://gitlab.freedesktop.org|Job failed \(system failure\)|Uploading artifacts.*Bad Gateway|ERROR: Uploading artifacts as "archive" to coordinator.*413 Request Entity Too Large|ERROR: Uploading artifacts as "archive" to coordinator... error.*couldn\'t execute POST against|HTTP request sent, awaiting response... 500 Internal Server Error|error: RPC failed;|section_end:1631042648:upload_artifacts_on_failure'
        ),
    ),
    MatcherAction(
        name="JOB_TIMEOUT",
        description="Test timed out",
        regex=re.compile(r"ERROR: Job failed: execution took longer than \\d+h\\d+m\\d+s seconds|ERROR: Job failed: execution took longer than"),
        requires_attention=True,
    ),
    MatcherAction(
        name="MINIO_ERROR",
        description="minio generic error",
        regex=re.compile(r"mcli: <ERROR>"),
    ),
    MatcherAction(
        name="NO_SPACE_LEFT",
        description="No space left",
        regex=re.compile(r"No space left on device"),
        requires_attention=True,
    ),
    MatcherAction(
        name="PDU_BUG",
        description="Failure interacting with the PDU",
        regex=re.compile(r"AttributeError: 'NoneType' object has no attribute 'set'|The function <function SnmpPDU.get_port_state .*> failed 3 times in a row"),
        requires_attention=True,
    ),
    MatcherAction(
        name="REGISTRATION_FAILURE",
        description="Machine tags mismatch",
        regex=re.compile(r"Mismatch for 'tags'"),
    ),
     MatcherAction(
        name="STALE_CLIENT_BUG",
        description="A client is already connected, re-try later!",
        regex=re.compile(r"A client is already connected, re-try later!"),
        requires_attention=True,
    ),
    MatcherAction(
        name="TARFILE_BUG_FAILURE",
        description="Job folder sync bug detected",
        regex=re.compile(r"tarfile.ReadError: file could not be opened successfully"),
        requires_attention=True,
    ),
    MatcherAction(
        name="TIMEOUT_NONE_BUG",
        description="Timeout none bug detected",
        regex=re.compile(r"AttributeError: 'NoneType' object has no attribute 'timeouts'"),
        requires_attention=True,
    ),
    MatcherAction(
        name="UNAUTHORIZED_USE_ERROR",
        description="Unauthorized user error",
        regex=re.compile(r"The machines exposed by this runner are only allowed to be used by Valve-authorized projects"),
        requires_attention=True,
    ),
    MatcherAction(
        name="WGET_404_ERROR",
        description="wget fetch failure",
        regex=re.compile(r"ERROR 404: Not Found"),
        requires_attention=True,
    ),
    MatcherAction(
        name="JOB_PROCESS_FAILED_TO_START",
        description="The Job Process failed to start",
        regex=re.compile(r'ERROR: Could not queue the job: "The job process failed to start"'),
        requires_attention=True,
    ),

    # TODO: Detect the dxvk-ci issue where VRAM is too small
]

class TriageResult:
    def __init__(self, job_log: str):
        self.matches = []

        for line in job_log.splitlines():
            for ma in MATCHER_TABLE:
                if match := ma.match_line(line):
                    self.matches.append(match)

    @cached_property
    def results(self):
        mas = {ma:[] for ma in MATCHER_TABLE}
        for match in self.matches:
            mas[match.matcher].append(match)
        return mas

    @cached_property
    def matched_patterns(self):
        return {m.matcher.name for m in self.matches}

    @property
    def has_known_issues(self):
        return len(self.matches) > 0

    @property
    def requires_attention(self):
        for match in self.matches:
            if match.matcher.requires_attention:
                return True
        return False
